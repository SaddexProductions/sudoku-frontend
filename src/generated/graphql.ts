import gql from 'graphql-tag';
import { Injectable } from '@angular/core';
import * as Apollo from 'apollo-angular';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};



export type Score = {
  __typename?: 'Score';
  _id: Scalars['ID'];
  score: Scalars['Int'];
  size: Scalars['Int'];
  username: Scalars['String'];
  date: Scalars['String'];
};

export type AddedScore = {
  __typename?: 'AddedScore';
  score: Score;
  position: Scalars['String'];
};

export type Result = {
  __typename?: 'Result';
  score: Scalars['Int'];
  meta: Scalars['String'];
};

export type AuthData = {
  __typename?: 'AuthData';
  user: User;
  token: Scalars['String'];
};

export type Game = {
  __typename?: 'Game';
  meta: Scalars['String'];
  board: Array<Array<Scalars['Int']>>;
  original: Array<Array<Scalars['Int']>>;
};

export type User = {
  __typename?: 'User';
  _id: Scalars['ID'];
  username: Scalars['String'];
  email: Scalars['String'];
  password?: Maybe<Scalars['String']>;
  darkMode: Scalars['Boolean'];
  defaultSize: Scalars['Int'];
  validResetToken?: Maybe<Scalars['String']>;
  passwordUpdatedOn?: Maybe<Scalars['String']>;
};

export type Message = {
  __typename?: 'Message';
  message: Scalars['String'];
};

export type Login = {
  __typename?: 'Login';
  loggedIn: Scalars['Boolean'];
};

export type Matches = {
  __typename?: 'Matches';
  matches: Scalars['Boolean'];
};

export type DarkModeSetting = {
  __typename?: 'DarkModeSetting';
  currentSetting: Scalars['Boolean'];
};

export type UserInput = {
  email: Scalars['String'];
  username: Scalars['String'];
  password: Scalars['String'];
};

export type ChangeUser = {
  username?: Maybe<Scalars['String']>;
  password: Scalars['String'];
  newPassword?: Maybe<Scalars['String']>;
  defaultSize?: Maybe<Scalars['Int']>;
  darkMode?: Maybe<Scalars['Boolean']>;
};

export type RootQuery = {
  __typename?: 'RootQuery';
  autoLogin?: Maybe<User>;
  checkEmail: Matches;
  checkResetToken: Message;
  checkUsername: Matches;
  login: AuthData;
  startGame: Game;
  endGame: Result;
  scores?: Maybe<Array<Score>>;
};


export type RootQueryCheckEmailArgs = {
  email: Scalars['String'];
};


export type RootQueryCheckResetTokenArgs = {
  resetToken: Scalars['String'];
};


export type RootQueryCheckUsernameArgs = {
  username: Scalars['String'];
};


export type RootQueryLoginArgs = {
  identifier: Scalars['String'];
  password: Scalars['String'];
};


export type RootQueryStartGameArgs = {
  size: Scalars['Int'];
};


export type RootQueryEndGameArgs = {
  meta: Scalars['String'];
  solution: Array<Array<Scalars['Int']>>;
};


export type RootQueryScoresArgs = {
  start: Scalars['Int'];
};

export type RootMutation = {
  __typename?: 'RootMutation';
  addScore?: Maybe<AddedScore>;
  createUser: User;
  deleteAccount: Message;
  editUser: AuthData;
  requestPasswordReset: Message;
  resetPassword: Message;
  setDarkMode: DarkModeSetting;
};


export type RootMutationAddScoreArgs = {
  gameToken: Scalars['String'];
};


export type RootMutationCreateUserArgs = {
  userInput?: Maybe<UserInput>;
};


export type RootMutationDeleteAccountArgs = {
  password?: Maybe<Scalars['String']>;
};


export type RootMutationEditUserArgs = {
  userChange?: Maybe<ChangeUser>;
};


export type RootMutationRequestPasswordResetArgs = {
  identifier: Scalars['String'];
};


export type RootMutationResetPasswordArgs = {
  newPassword: Scalars['String'];
  resetToken: Scalars['String'];
};


export type RootMutationSetDarkModeArgs = {
  darkMode: Scalars['Boolean'];
};

export type SignupMutationVariables = Exact<{
  username: Scalars['String'];
  email: Scalars['String'];
  password: Scalars['String'];
}>;


export type SignupMutation = (
  { __typename?: 'RootMutation' }
  & { createUser: (
    { __typename?: 'User' }
    & Pick<User, '_id' | 'email' | 'username'>
  ) }
);

export type EditUserMutationVariables = Exact<{
  password: Scalars['String'];
  username?: Maybe<Scalars['String']>;
  newPassword?: Maybe<Scalars['String']>;
  size?: Maybe<Scalars['Int']>;
  darkMode?: Maybe<Scalars['Boolean']>;
}>;


export type EditUserMutation = (
  { __typename?: 'RootMutation' }
  & { editUser: (
    { __typename?: 'AuthData' }
    & Pick<AuthData, 'token'>
    & { user: (
      { __typename?: 'User' }
      & Pick<User, '_id' | 'email' | 'username' | 'darkMode' | 'defaultSize'>
    ) }
  ) }
);

export type RequestPasswordResetMutationVariables = Exact<{
  identifier: Scalars['String'];
}>;


export type RequestPasswordResetMutation = (
  { __typename?: 'RootMutation' }
  & { requestPasswordReset: (
    { __typename?: 'Message' }
    & Pick<Message, 'message'>
  ) }
);

export type ResetPasswordMutationVariables = Exact<{
  newPassword: Scalars['String'];
  resetToken: Scalars['String'];
}>;


export type ResetPasswordMutation = (
  { __typename?: 'RootMutation' }
  & { resetPassword: (
    { __typename?: 'Message' }
    & Pick<Message, 'message'>
  ) }
);

export type AddScoreMutationVariables = Exact<{
  gameToken: Scalars['String'];
}>;


export type AddScoreMutation = (
  { __typename?: 'RootMutation' }
  & { addScore?: Maybe<(
    { __typename?: 'AddedScore' }
    & Pick<AddedScore, 'position'>
    & { score: (
      { __typename?: 'Score' }
      & Pick<Score, '_id' | 'score' | 'size' | 'username' | 'date'>
    ) }
  )> }
);

export type DeleteUserMutationVariables = Exact<{
  password: Scalars['String'];
}>;


export type DeleteUserMutation = (
  { __typename?: 'RootMutation' }
  & { deleteAccount: (
    { __typename?: 'Message' }
    & Pick<Message, 'message'>
  ) }
);

export type SetDarkModeMutationVariables = Exact<{
  darkMode: Scalars['Boolean'];
}>;


export type SetDarkModeMutation = (
  { __typename?: 'RootMutation' }
  & { setDarkMode: (
    { __typename?: 'DarkModeSetting' }
    & Pick<DarkModeSetting, 'currentSetting'>
  ) }
);

export type CheckResetTokenQueryVariables = Exact<{
  resetToken: Scalars['String'];
}>;


export type CheckResetTokenQuery = (
  { __typename?: 'RootQuery' }
  & { checkResetToken: (
    { __typename?: 'Message' }
    & Pick<Message, 'message'>
  ) }
);

export type AutoLoginQueryVariables = Exact<{ [key: string]: never; }>;


export type AutoLoginQuery = (
  { __typename?: 'RootQuery' }
  & { autoLogin?: Maybe<(
    { __typename?: 'User' }
    & Pick<User, '_id' | 'username' | 'email' | 'darkMode' | 'defaultSize'>
  )> }
);

export type LoginQueryVariables = Exact<{
  identifier: Scalars['String'];
  password: Scalars['String'];
}>;


export type LoginQuery = (
  { __typename?: 'RootQuery' }
  & { login: (
    { __typename?: 'AuthData' }
    & Pick<AuthData, 'token'>
    & { user: (
      { __typename?: 'User' }
      & Pick<User, '_id' | 'username' | 'email' | 'darkMode' | 'defaultSize'>
    ) }
  ) }
);

export type StartGameQueryVariables = Exact<{
  size: Scalars['Int'];
}>;


export type StartGameQuery = (
  { __typename?: 'RootQuery' }
  & { startGame: (
    { __typename?: 'Game' }
    & Pick<Game, 'meta' | 'board' | 'original'>
  ) }
);

export type EndGameQueryVariables = Exact<{
  meta: Scalars['String'];
  solution: Array<Array<Scalars['Int']>>;
}>;


export type EndGameQuery = (
  { __typename?: 'RootQuery' }
  & { endGame: (
    { __typename?: 'Result' }
    & Pick<Result, 'meta' | 'score'>
  ) }
);

export type GetScoresQueryVariables = Exact<{
  start: Scalars['Int'];
}>;


export type GetScoresQuery = (
  { __typename?: 'RootQuery' }
  & { scores?: Maybe<Array<(
    { __typename?: 'Score' }
    & Pick<Score, '_id' | 'score' | 'size' | 'username' | 'date'>
  )>> }
);

export type CheckEmailQueryVariables = Exact<{
  email: Scalars['String'];
}>;


export type CheckEmailQuery = (
  { __typename?: 'RootQuery' }
  & { checkEmail: (
    { __typename?: 'Matches' }
    & Pick<Matches, 'matches'>
  ) }
);

export type CheckUsernameQueryVariables = Exact<{
  username: Scalars['String'];
}>;


export type CheckUsernameQuery = (
  { __typename?: 'RootQuery' }
  & { checkUsername: (
    { __typename?: 'Matches' }
    & Pick<Matches, 'matches'>
  ) }
);

export const SignupDocument = gql`
    mutation Signup($username: String!, $email: String!, $password: String!) {
  createUser(userInput: {username: $username, email: $email, password: $password}) {
    _id
    email
    username
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class SignupGQL extends Apollo.Mutation<SignupMutation, SignupMutationVariables> {
    document = SignupDocument;
    
  }
export const EditUserDocument = gql`
    mutation EditUser($password: String!, $username: String, $newPassword: String, $size: Int, $darkMode: Boolean) {
  editUser(userChange: {password: $password, username: $username, newPassword: $newPassword, defaultSize: $size, darkMode: $darkMode}) {
    user {
      _id
      email
      username
      darkMode
      defaultSize
    }
    token
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class EditUserGQL extends Apollo.Mutation<EditUserMutation, EditUserMutationVariables> {
    document = EditUserDocument;
    
  }
export const RequestPasswordResetDocument = gql`
    mutation RequestPasswordReset($identifier: String!) {
  requestPasswordReset(identifier: $identifier) {
    message
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class RequestPasswordResetGQL extends Apollo.Mutation<RequestPasswordResetMutation, RequestPasswordResetMutationVariables> {
    document = RequestPasswordResetDocument;
    
  }
export const ResetPasswordDocument = gql`
    mutation ResetPassword($newPassword: String!, $resetToken: String!) {
  resetPassword(newPassword: $newPassword, resetToken: $resetToken) {
    message
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class ResetPasswordGQL extends Apollo.Mutation<ResetPasswordMutation, ResetPasswordMutationVariables> {
    document = ResetPasswordDocument;
    
  }
export const AddScoreDocument = gql`
    mutation AddScore($gameToken: String!) {
  addScore(gameToken: $gameToken) {
    score {
      _id
      score
      size
      username
      date
    }
    position
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class AddScoreGQL extends Apollo.Mutation<AddScoreMutation, AddScoreMutationVariables> {
    document = AddScoreDocument;
    
  }
export const DeleteUserDocument = gql`
    mutation DeleteUser($password: String!) {
  deleteAccount(password: $password) {
    message
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class DeleteUserGQL extends Apollo.Mutation<DeleteUserMutation, DeleteUserMutationVariables> {
    document = DeleteUserDocument;
    
  }
export const SetDarkModeDocument = gql`
    mutation SetDarkMode($darkMode: Boolean!) {
  setDarkMode(darkMode: $darkMode) {
    currentSetting
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class SetDarkModeGQL extends Apollo.Mutation<SetDarkModeMutation, SetDarkModeMutationVariables> {
    document = SetDarkModeDocument;
    
  }
export const CheckResetTokenDocument = gql`
    query CheckResetToken($resetToken: String!) {
  checkResetToken(resetToken: $resetToken) {
    message
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class CheckResetTokenGQL extends Apollo.Query<CheckResetTokenQuery, CheckResetTokenQueryVariables> {
    document = CheckResetTokenDocument;
    
  }
export const AutoLoginDocument = gql`
    query AutoLogin {
  autoLogin {
    _id
    username
    email
    darkMode
    defaultSize
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class AutoLoginGQL extends Apollo.Query<AutoLoginQuery, AutoLoginQueryVariables> {
    document = AutoLoginDocument;
    
  }
export const LoginDocument = gql`
    query Login($identifier: String!, $password: String!) {
  login(identifier: $identifier, password: $password) {
    user {
      _id
      username
      email
      darkMode
      defaultSize
    }
    token
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class LoginGQL extends Apollo.Query<LoginQuery, LoginQueryVariables> {
    document = LoginDocument;
    
  }
export const StartGameDocument = gql`
    query StartGame($size: Int!) {
  startGame(size: $size) {
    meta
    board
    original
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class StartGameGQL extends Apollo.Query<StartGameQuery, StartGameQueryVariables> {
    document = StartGameDocument;
    
  }
export const EndGameDocument = gql`
    query EndGame($meta: String!, $solution: [[Int!]!]!) {
  endGame(meta: $meta, solution: $solution) {
    meta
    score
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class EndGameGQL extends Apollo.Query<EndGameQuery, EndGameQueryVariables> {
    document = EndGameDocument;
    
  }
export const GetScoresDocument = gql`
    query GetScores($start: Int!) {
  scores(start: $start) {
    _id
    score
    size
    username
    date
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class GetScoresGQL extends Apollo.Query<GetScoresQuery, GetScoresQueryVariables> {
    document = GetScoresDocument;
    
  }
export const CheckEmailDocument = gql`
    query CheckEmail($email: String!) {
  checkEmail(email: $email) {
    matches
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class CheckEmailGQL extends Apollo.Query<CheckEmailQuery, CheckEmailQueryVariables> {
    document = CheckEmailDocument;
    
  }
export const CheckUsernameDocument = gql`
    query CheckUsername($username: String!) {
  checkUsername(username: $username) {
    matches
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class CheckUsernameGQL extends Apollo.Query<CheckUsernameQuery, CheckUsernameQueryVariables> {
    document = CheckUsernameDocument;
    
  }