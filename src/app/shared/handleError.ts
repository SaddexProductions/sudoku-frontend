export const handleErr = (err: any) => {
    if (err.networkError && err.networkError.error.errors) {
        return err.networkError.error.errors[0].message;
    } else {
        return "Network Error";
    }
}