import { ValidatorFn, FormGroup } from '@angular/forms';

export const equalPasswordsValidator: ValidatorFn = (control: FormGroup): null => {
    
    const password = control.get("password");
    const repeatPassword = control.get("repeatPassword");

    if(password.value !== repeatPassword.value) {

        repeatPassword.setErrors({mustMatch: true});
    } else {
        repeatPassword.setErrors(null);
    }

    return null;
}