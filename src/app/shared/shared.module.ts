import { NgModule } from '@angular/core';
import { LoaderComponent } from '../components/loader/loader.component';
import { HeaderComponent } from '../components/header/header.component';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { BackdropComponent } from '../components/backdrop/backdrop.component';
import { MenuComponent } from '../components/menu/menu.component';
import { ToggleComponent } from '../components/toggle/toggle.component';
import {ErrorComponent} from '../components/error/error.component';
import { BignumberPipe } from '../components/generalPipes/bignumber.pipe';

@NgModule({
    declarations: [
        ErrorComponent,
        LoaderComponent,
        HeaderComponent,
        BackdropComponent,
        MenuComponent,
        ToggleComponent,
        BignumberPipe
    ],
    imports: [
        CommonModule,
        RouterModule,
        FontAwesomeModule
    ],
    exports: [
        ErrorComponent,
        LoaderComponent,
        HeaderComponent,
        BackdropComponent,
        MenuComponent,
        ToggleComponent,
        BignumberPipe
    ]
})
export class SharedModule {

}