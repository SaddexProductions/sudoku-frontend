import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';


const routes: Routes = [
  {path: '', loadChildren: () => import ('./views/game/game.module').then(m => m.GameModule)},
  {path: 'auth', loadChildren: () => import ('./views/auth/auth.module').then(m => m.AuthModule)},
  {path: 'scoreboard', loadChildren: () => import ('./views/scoreboard/scoreboard.module').then(m => m.ScoreBoardModule)},
  {path: 'settings', loadChildren: () => import ('./views/settings/settings.module').then(m => m.SettingsModule)},
  {
		path: "**",
		redirectTo: ""
	}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    preloadingStrategy: PreloadAllModules
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
