import { Component, OnDestroy, OnInit } from '@angular/core';
import { AuthService, Status } from './views/auth/auth.service';
import { Subscription } from 'rxjs';
import { UIService } from './components/menu/ui.service';
import { User } from './views/auth/user.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {

  private authSub: Subscription;
  first: boolean = true;
  user: User;

  constructor(private authService: AuthService,
    private uiService: UIService) {
  }

  ngOnInit (): void {
    this.authService.autoLogin();

    this.authSub = this.authService.status.subscribe((status: Status) => {
      this.first = status.first;

      this.user = status.user;
    });
  }

  ngOnDestroy (): void {
    this.authSub.unsubscribe();

  }

  closeSubMenu (): void {
    this.uiService.setSubMenu(false);
  }
}
