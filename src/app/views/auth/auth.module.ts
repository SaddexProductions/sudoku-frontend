import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AuthComponent } from './auth.component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { SharedModule } from 'src/app/shared/shared.module';
import { ResetpasswordComponent } from './resetpassword/resetpassword.component';

@NgModule({
    declarations: [
        AuthComponent,
        LoginComponent,
        SignupComponent,
        ResetpasswordComponent,
    ],
    imports: [
        CommonModule,
        FormsModule,
        SharedModule,
        FontAwesomeModule,
        ReactiveFormsModule,
        RouterModule.forChild([
            {path: '', component: AuthComponent, children: [
                {path: '', component: LoginComponent},
                {path: 'signup', component: SignupComponent},
                {path: 'resetpassword', component: ResetpasswordComponent}
            ]}
        ])
    ]
})
export class AuthModule {

}