import { Injectable } from '@angular/core';
import { BehaviorSubject, of, Observable, Subscription } from 'rxjs';
import { User } from './user.model';
import { Router } from '@angular/router';
import { LoginGQL, 
    RequestPasswordResetGQL, 
    SignupGQL, CheckEmailGQL, 
    AutoLoginGQL, 
    SetDarkModeGQL, 
    ResetPasswordGQL, 
    CheckUsernameGQL, 
    CheckResetTokenGQL,
    DeleteUserGQL,
    EditUserGQL} from 'src/generated/graphql';
import { catchError, map } from 'rxjs/operators';
import { FormControl, ValidationErrors, AsyncValidatorFn } from '@angular/forms';
import { handleErr } from 'src/app/shared/handleError';
import { getToken } from 'src/app/shared/getToken';
import { GameService } from '../game/game.service';

export interface Status {
    user: User;
    first: boolean;
    deleteError: string;
    login: boolean;
    error: string;
    info: string;
    loading: boolean;
}

interface AuthData {
    token: string;
    user: User;
}

export interface EditUserArgument {
    password: string;
    username?: string;
    newPassword?: string;
    size?: number;
    darkMode?: boolean;
}

@Injectable({ providedIn: 'root' })
export class AuthService {

    readonly status = new BehaviorSubject<Status>({
        user: null,
        deleteError: "",
        first: true,
        login: false,
        error: "",
        info: "",
        loading: false
    });

    constructor(private router: Router,
        private gameService: GameService,
        private loginGQL: LoginGQL,
        private requestPWResetGQL: RequestPasswordResetGQL,
        private signupGQL: SignupGQL,
        private checkEmailGQL: CheckEmailGQL,
        private checkUsernameGQL: CheckUsernameGQL,
        private checkResetTokenGQL: CheckResetTokenGQL,
        private editUserGQL: EditUserGQL,
        private deleteAccountGQL: DeleteUserGQL,
        private autoLoginGQL: AutoLoginGQL,
        private toggleDarkModeGQL: SetDarkModeGQL,
        private resetPasswordGQL: ResetPasswordGQL
        ) {

    }

   
    emailAsyncValidator: AsyncValidatorFn = (control: FormControl): Promise<ValidationErrors> | Observable<ValidationErrors> => {
        return this.checkEmailGQL.fetch({email: control.value}).pipe(map(result => {
            return result.data.checkEmail.matches ? {emailExists: true} : null
            }), catchError((_2: any) => {
                return of({asyncValidationFailed: true})
            }
        ))
    }

    usernameAsyncValidator: AsyncValidatorFn = (control: FormControl): Promise<ValidationErrors> | Observable<ValidationErrors> => {
        return this.checkUsernameGQL.fetch({username: control.value}).pipe(map(result => {
            return result.data.checkUsername.matches ? {userNameExists: true} : null
            }), catchError((_2: any) => {
                return of({asyncValidationFailed: true})
            }
        ))
    }

    autoLogin(): void {

        const token = getToken();
        this.status.next({
            ...this.status.value,
            login: !!token
        });

        this.autoLoginGQL.fetch().pipe(map(result => result.data.autoLogin),
        catchError(_ => of(false)))
        .subscribe((data: User | boolean | null) => {

            this.status.next({
                ...this.status.value,
                login: !!data,
                user: typeof data !== "boolean" ? data : null,
                first: false
            });

            if(token && !data) {
                this.router.navigate(['/auth']);
            }
        });
    }

    checkResetToken (resetToken: string): Observable<boolean> {
        return this.checkResetTokenGQL.fetch({resetToken})
        .pipe(map(result => result.data.checkResetToken.message),
        catchError(_ => of(null)));
    }

    deleteAccount (password: string): void {

        this.status.next({
            ...this.status.value,
            deleteError: ""
        });

        let error = false;
        this.deleteAccountGQL.mutate({password})
        .pipe(map(result => result.data.deleteAccount.message),
        catchError(err => {
            error = true;
            return of(handleErr(err))
        }))
        .subscribe((finalResult: string) => {

            if(error) {
                this.status.next({
                    ...this.status.value,
                    deleteError: finalResult
                });
            } else {
                this.status.next({
                    ...this.status.value,
                    info: finalResult
                });

                this.logout();
            }
        });
    }

    editUser (objToSend: EditUserArgument): void {

        this.setLoading();

        this.editUserGQL.mutate(objToSend)
        .pipe(map(result => result.data.editUser),
        catchError(err => of(handleErr(err))))
        .subscribe((finalResult: AuthData | string) => {

            if(typeof finalResult === "string") {
                this.setError(finalResult);
            } else {
                this.status.next({
                    ...this.status.value,
                    loading: false,
                    user: finalResult.user,
                    info: "Settings saved, redirecting..."
                });

                this.saveToken(finalResult.token);
            }
        });
    }

    forgetPassword(identifier: string): void {

        let error = false;
        this.setLoading();

        this.requestPWResetGQL.mutate({ identifier }).pipe(map(result =>
            result.data.requestPasswordReset.message
        ), catchError(err => {
            error = true;
            return of(handleErr(err)
            )
        }))
            .subscribe((message: string) => {

                if (!error) {
                    this.status.next({
                        ...this.status.value,
                        loading: false,
                        info: message
                    });
                } else {
                    this.setError(message);
                }
            });
    }

    login(identifier: string, password: string): void {
        this.setLoading();

        this.loginGQL.fetch({ identifier, password }).pipe(map(result =>
            result.data
        ), catchError(
            err => of(handleErr(err))
        )).subscribe(
            (finalResult: string | { login: AuthData }) => {
                if (typeof finalResult !== "string") {
                    this.status.next({
                        ...this.status.value,
                        loading: false,
                        user: finalResult.login.user,
                        login: true
                    });

                    this.saveToken(finalResult.login.token);
                } else {
                    this.setError(finalResult);
                }
            }
        );
    }

    logout(): void {
        this.gameService.stopGame();

        this.status.next({
            ...this.status.value,
            login: false,
            user: null
        });
        document.cookie = "token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
        this.router.navigate(['/auth']);
    }

    resetDeleteError (): void {
        this.status.next({
            ...this.status.value,
            deleteError: ""
        });
    }

    resetError(): void {
        this.status.next({
            ...this.status.value,
            error: ""
        });
    }

    resetPassword (password: string, resetToken: string): void {
        this.status.next({
            ...this.status.value,
            loading: true
        });

        let error = false;

        this.resetPasswordGQL.mutate({newPassword: password, resetToken})
        .pipe(map(result => result.data.resetPassword.message),
        catchError(err => {
            error = true;
            return of(handleErr(err));
        }))
        .subscribe((finalResult:string) => {

            if(!error) {
                this.status.next({
                    ...this.status.value,
                    loading: false,
                    info: finalResult
                });
            } else {
                this.setError(finalResult);
            }
        });
    }

    resetInfo(): void {
        this.status.next({
            ...this.status.value,
            info: ""
        });
    }

    saveToken(token): void {
        let d = new Date();
        d.setTime(d.getTime() + (30 * 24 * 60 * 60 * 1000)); //cookie stored for 30 days
        const expires = "expires=" + d.toUTCString();
        document.cookie = "token =" + token +
            ";" + expires + ";path=/";
    }

    setError (message: string): void {
        this.status.next({
            ...this.status.value,
            error: message,
            loading: false
        });
    }

    setLoading (): void {
        this.status.next({
            ...this.status.value,
            loading: true
        });
    }

    signUp (email: string, password: string, username: string): void {
        this.status.next({
            ...this.status.value,
            loading: true
        });

        this.signupGQL.mutate({email, password, username})
        .pipe(map(result => result.data.createUser),
        catchError(
            err =>  of(handleErr(err))
        )).subscribe((finalResult: string | User ) => {
            if (typeof finalResult !== "string") {
                this.status.next({
                    ...this.status.value,
                    loading: false,
                    info: `User ${finalResult.username} successfully created. Redirecting to login...`
                });

                setTimeout(() => {
                    this.router.navigate(['/auth']);
                }, 3500);
            } else {
                this.setError(finalResult);
            }
        })
    }

    toggleDarkMode(mode: boolean): void {
        this.toggleDarkModeGQL.mutate({darkMode: mode})
        .pipe(
            map(result => result.data.setDarkMode.currentSetting),
            catchError(err => {
                return of(handleErr(err))
            })
        )
        .subscribe((finalResult: string | boolean) => {

            if(typeof finalResult === "boolean") {
                this.status.next({
                    ...this.status.value,
                    user: {
                        ...this.status.value.user,
                        darkMode: finalResult
                    }
                })
            } else {
                this.setError(finalResult);
            }
        });
    }
}