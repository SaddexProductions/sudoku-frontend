import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';
import { equalPasswordsValidator } from 'src/app/shared/equalPasswords';
import { Subscription } from 'rxjs';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit, OnDestroy {

  private authStatusSub: Subscription;
  loading: boolean = false;
  signupForm: FormGroup;

  constructor(private authService: AuthService,
    private titleService: Title) {
  }

  ngOnInit(): void {
    this.titleService.setTitle("Sign up - Sudoku");
    
    this.initForm();

    this.authStatusSub = this.authService.status.subscribe(
      authStatus => {
        this.loading = authStatus.loading;

        if(authStatus.info && authStatus.info.length > 0) {
          this.signupForm.reset();
        }
      }
    );
  }

  ngOnDestroy(): void {
    this.authService.resetError();
    this.authStatusSub.unsubscribe();
  }

  private initForm () {
    this.signupForm = new FormGroup({
      'email': new FormControl(null, [Validators.required, Validators.email, Validators.maxLength(120)], 
      this.authService.emailAsyncValidator),
      'password': new FormControl(null, 
        [Validators.required, Validators.minLength(6), Validators.maxLength(25), 
          Validators.pattern(/^(?:(?=.*[a-z])(?:(?=.*[A-Z])(?=.*[\d\W])|(?=.*\W)(?=.*\d))|(?=.*\W)(?=.*[A-Z])(?=.*\d)).{5,25}$/)]),
      'repeatPassword': new FormControl(null, [Validators.required]),
      'username': new FormControl(null, [Validators.required, 
        Validators.minLength(3), 
        Validators.maxLength(20),
        Validators.pattern(/^(?=.{3,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$/)], 
      this.authService.usernameAsyncValidator)
    }, {validators: equalPasswordsValidator});
  }

  signup () {
    this.authService.resetError();
    this.authService.signUp(this.signupForm.value.email, this.signupForm.value.password, this.signupForm.value.username);
  }

}
