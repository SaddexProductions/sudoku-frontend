export class User {
    constructor(public email: string, public _id: string, public username, public darkMode: boolean, public defaultSize: number) {
    }
}