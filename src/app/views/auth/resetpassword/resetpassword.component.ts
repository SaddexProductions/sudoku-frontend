import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { equalPasswordsValidator } from 'src/app/shared/equalPasswords';
import { AuthService, Status } from '../auth.service';
import { Route, ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.css']
})
export class ResetpasswordComponent implements OnInit, OnDestroy {

  private authSub: Subscription;
  private routerSub: Subscription;
  private checkResetTokenSub: Subscription;
  loading: boolean = false;
  parameter: string = "";
  resetForm: FormGroup;
  token: string = "";

  constructor(private authService: AuthService, 
    private route: ActivatedRoute, 
    private router: Router,
    private titleService: Title) {
   }

  ngOnInit(): void {
    this.initForm();

    
    this.titleService.setTitle("Reset Password - Sudoku");

    this.routerSub = this.route.queryParams.subscribe(param => {
      
      if(param && param.token) {
        this.token = param.token;
      }

      let validToken = false;

      this.checkResetTokenSub = this.authService.checkResetToken(this.token).subscribe(
        valid => {
          validToken = valid;
          if(!validToken) this.router.navigate(['/auth']);
        }
      );
    });

    this.authSub = this.authService.status.subscribe((status: Status) => {
      this.loading = status.loading;

      if(status.info) {
        setTimeout(() => {
          this.router.navigate(['/auth']);
        }, 3500);
      }
    });
  }

  private initForm (): void {
    this.resetForm = new FormGroup({
      'password': new FormControl(null, [Validators.required, Validators.minLength(6), Validators.maxLength(25), 
        Validators.pattern(/^(?:(?=.*[a-z])(?:(?=.*[A-Z])(?=.*[\d\W])|(?=.*\W)(?=.*\d))|(?=.*\W)(?=.*[A-Z])(?=.*\d)).{5,25}$/)]),
      'repeatPassword': new FormControl(null, [Validators.required])
    }, {validators: equalPasswordsValidator});
  }

  resetPassword (): void {
    this.authService.resetError();
    this.authService.resetPassword(this.resetForm.value.password, this.token);
  }

  ngOnDestroy (): void {
    this.routerSub.unsubscribe();
    this.checkResetTokenSub.unsubscribe();
    this.authSub.unsubscribe();

    this.authService.resetError();
  }

}
