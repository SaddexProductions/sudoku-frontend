import { Component, OnDestroy, OnInit } from '@angular/core';
import { AuthService } from './auth.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { UIService } from 'src/app/components/menu/ui.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit, OnDestroy {

  private authStatusSub: Subscription;
  errorMessage: string = "";
  infoMessage: string = "";
  height: number = window.innerHeight;
  loading: boolean;
  private uiSub: Subscription;

  constructor(private authService: AuthService, 
    private router: Router,
    private uiService: UIService) {
  }

  ngOnInit (): void {
    this.authStatusSub = this.authService.status.subscribe(status => {
      this.loading = status.loading;

      this.infoMessage = status.info || "";

      this.errorMessage = status.error || "";

      if (this.infoMessage.length > 0) {

        setTimeout(() => {
          this.authService.resetInfo();
        }, 3000);
      }

      if(status.login) this.router.navigate(['/']);
    });

    this.uiSub = this.uiService.status.subscribe(({height}) => {

      this.height = height;
    });
  }

  ngOnDestroy(): void {
    this.authStatusSub.unsubscribe();
    this.uiSub.unsubscribe();
  }

}
