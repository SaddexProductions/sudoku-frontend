import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';
import { Subscription } from 'rxjs';
import { Title } from '@angular/platform-browser';
import { ScoreService } from '../../scoreboard/scoreboard.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {

  login: boolean;
  private authStatusSub: Subscription;
  errorMessage: string;
  loginForm: FormGroup;
  loading: boolean;
  next: boolean = false;

  constructor(private authService: AuthService, private scoreService: ScoreService,
    private titleService: Title) {
  }

  private initForm(): void {
    this.loginForm = new FormGroup({
      'identifier': new FormControl(null, [Validators.required, Validators.minLength(5)]),
      'password': new FormControl(null, Validators.required)
    });
  }

  onSubmit (): void {
    if(!this.next) {
      if(this.loginForm.controls.identifier.valid) {
        this.next = true;
      }
      return;
    }
    this.authService.resetError();
    this.authService.login(this.loginForm.value.identifier, this.loginForm.value.password);
  }

  onForgetPassword (): void {
    this.authService.resetError();
    this.authService.forgetPassword(this.loginForm.value.identifier);
  }

  toPassword(): void {
    this.next = true;
    this.authService.resetError();
  }

  ngOnInit(): void {
    this.initForm();

    this.titleService.setTitle("Sudoku");

    this.authStatusSub = this.authService.status.subscribe(
      authStatus => {
        this.login = authStatus.login;
        this.errorMessage = authStatus.error || "";

        if(this.errorMessage && this.errorMessage.length > 0) {
          this.next = false;
          this.loginForm.reset();
        }

        this.loading = authStatus.loading;

        if(authStatus.info) this.scoreService.clearScores();

      }
    );
  }

  ngOnDestroy(): void {
    this.authStatusSub.unsubscribe();
    this.authService.resetError();
    this.authService.resetInfo();
  }

}
