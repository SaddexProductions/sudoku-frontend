import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, UrlTree } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { map, take } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {

    constructor(private authService: AuthService, private router: Router) {
    }

    canActivate(_: ActivatedRouteSnapshot, 
        _2: RouterStateSnapshot):
         boolean | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | UrlTree {
        return this.authService.status.pipe(
            take(1),
            map(status => {
            const isAuth = status.login;

            if (isAuth) return true;

            return this.router.createUrlTree(['/auth']);
        }))
    }
}