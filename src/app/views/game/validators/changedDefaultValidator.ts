import { ValidatorFn, AbstractControl } from '@angular/forms';

export function changedDefaultValidator (char: string): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {

        if(char === " ") return null;

        if(control.value.toUpperCase() !== char) {
            return {defaultChanged: true};
        } else {
            return null;
        }
    };
}