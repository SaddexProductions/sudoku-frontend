import { Board } from '../../game.component';

export const getX = (name: string): number => +name.split('_')[1].split('-')[0];

export const getY = (name: string): number => +name.split('_')[1].split('-')[1];

export const getBaseValue = (input: number, base: number): number => input - input % base;


export const onlyUnique = (newArray: string[], value: string) => {
    let count: number = 0;

    for(const v of newArray) {
        if(v === value) count++;

        if(count >= 2) break;
    }
    return count <= 1;
}

export const verticalValidation = (staticIndex: number, board: Board[][], value: string): boolean => {

    const newArray = [];

    for (let i = 0; i < board.length; i++) {
        newArray.push(board[i][staticIndex].value);
    }

    return onlyUnique(newArray, value);
}

export const getCellArray = (x: number, 
    y: number, base: number, 
    board: Board[][], 
    returnIndexes: boolean = false): string[][] => {
        
    const newArray: string[] = [];
    const indexes: string[] = [];

    const baseX = getBaseValue(x, base);
    const baseY = getBaseValue(y, base);

    for (let i = baseX; i < baseX + base; i++) {

        for (let j = baseY; j < baseY + base; j++) {

            newArray.push(board[i][j].value);
            if(returnIndexes) indexes.push(`field_${i}-${j}`);
        }
    }

    return [newArray, indexes];
}

export const cellValidation = (newArray: string[], value: string): boolean => {

    return onlyUnique(newArray, value);
}

export const horizonalValidation = (staticIndex: number, board: Board[][], value: string): boolean => {

    const newArray = board[staticIndex].map(s => s.value);

    return onlyUnique(newArray, value);
}

