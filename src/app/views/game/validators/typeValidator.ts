import { ValidatorFn, AbstractControl } from '@angular/forms';

export function typeRangeValidator(chars: string[]): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {

        const forbidden = chars.indexOf(control.value) === -1;
        return forbidden ? { forbiddenCharacter: { value: control.value } } : null;
    };
}