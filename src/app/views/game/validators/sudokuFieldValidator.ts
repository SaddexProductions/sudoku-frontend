//npm modules
import { AbstractControl, ValidatorFn, FormGroup } from '@angular/forms';
import { getX, getY, horizonalValidation, verticalValidation, getCellArray, cellValidation } from './utility/utility';
import { Board } from '../game.component';

function sudokuFieldValidator (board: Board[][], base: number, fullControls: FormGroup): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {

        const name = getControlName(control);
        const x: number = getX(name);
        const y: number = getY(name);

        const othersToValidate: string[] = [];

        const [oldCellArray, indexes] = getCellArray(x, y, base, board, true);

        if(control.value !== board[x][y].value) {
            const oldValue = board[x][y].value;

            for(let j = 0; j < board[x].length; j++) {


                if(board[x][j].rowError && j !== y && board[x][j].value === oldValue) {

                    othersToValidate.push(`field_${x}-${j}`);
                }
            }

            for(let i = 0; i < board.length; i++) {

                if(board[i][y].columnError && i !== x && board[i][y].value === oldValue) {

                    othersToValidate.push(`field_${i}-${y}`);
                }
            }

            const translatedIndex = ((x % base)*base) + (y % base);

            for(let i = 0; i < oldCellArray.length; i++) {

                const m = getX(indexes[i]);
                const n = getY(indexes[i]);

                if(board[m][n].cellError &&
                oldCellArray[translatedIndex] === oldValue &&
                m !== x && n !== y
                ) {
                    othersToValidate.push(indexes[i]);
                }
            }
        }

        board[x][y].value = control.value;

        if(othersToValidate.length > 0) {
            
            for(const key of othersToValidate) {
                fullControls.controls[key].updateValueAndValidity();
            }
        }

        if(!control.value) return null;

        const horizontalPassed = horizonalValidation(x, board, control.value);

        board[x][y].rowError = !horizontalPassed;

        const verticalPassed = verticalValidation(y, board, control.value);

        board[x][y].columnError = !horizontalPassed;

        const cellArray = getCellArray(x, y, base, board)[0];
        const cellPassed = cellValidation(cellArray, control.value);

        board[x][y].cellError = !cellPassed;

        return horizontalPassed && verticalPassed && cellPassed ? null : {rowColumnCellInvalid: true};
    };
}

const getControlName = (c: AbstractControl): string | null => {
    const formGroup = c.parent.controls;
    return Object.keys(formGroup).find(name => c === formGroup[name]) || null;
}

export default sudokuFieldValidator;