import { Injectable } from '@angular/core';
import { BehaviorSubject, of, Subscription } from 'rxjs';
import { StartGameGQL, EndGameGQL, AddScoreGQL, Score } from 'src/generated/graphql';
import { catchError, map } from 'rxjs/operators';
import { handleErr } from 'src/app/shared/handleError';
import { ScoreService } from '../scoreboard/scoreboard.service';

export interface Game {
    meta: string;
    board: number[][];
    original: number[][];
}

interface GameStatus {
    base: number;
    counter: number;
    error: string;
    info: string;
    game: Game;
    loading: boolean;
    finalResult: string,
    score: number;
    success: boolean;
}

@Injectable({providedIn: 'root'})
export class GameService {

    readonly status = new BehaviorSubject<GameStatus>({
        base: 0,
        counter: 0,
        error: "",
        info: "",
        game: null,
        loading: false,
        finalResult: "",
        score: 0,
        success: false
    });

    private interval;
    private gameSub: Subscription;

    constructor (private startGameGQL: StartGameGQL, 
        private endGameGQL: EndGameGQL,
        private addScoreGQL: AddScoreGQL,
        private scoreService: ScoreService){
    }

    editBoard(newBoard: number[][]): void {
        this.status.next({
            ...this.status.value,
            game: {
                ...this.status.value.game,
                board: newBoard
            }
        })
    }

    resetError (): void {
        this.status.next({
            ...this.status.value,
            error: ""
        });
    }

    setError(errorMessage: string): void {
        this.status.next({
            ...this.status.value,
            error: errorMessage,
            loading: false
        });
    }

    startGame (base: number = 3): void {

        this.status.next({
            ...this.status.value,
            error: "",
            loading: true,
            base,
            score: 0
        });
        
        this.gameSub = this.startGameGQL.watch({size: base}).valueChanges
        .pipe(
            map(result => result.data.startGame),
            catchError(err => of(handleErr(err)))
        ).subscribe((result: Game | string) => {

            if(typeof result === "string") {
                this.setError(result);
            } else {
                this.status.next({
                    ...this.status.value,
                    error: "",
                    loading: false,
                    game: result
                });

                this.updateCounter();
            }
        });
    }

    stopGame (): void {
        if(this.gameSub) this.gameSub.unsubscribe();

        this.status.next({
            ...this.status.value,
            error: "",
            game: null,
            info: "",
            finalResult: "",
            counter: 0,
            score: 0,
            success: false
        });

        clearInterval(this.interval);
    }

    submitForm(): void {

        this.status.next({
            ...this.status.value,
            error: "",
            loading: true
        });

        this.endGameGQL.fetch({
            meta: this.status.value.game.meta, 
            solution: this.status.value.game.board
        }).pipe(map(result => result.data.endGame),
            catchError(err => of(handleErr(err))
            )
        ).subscribe((finalResult: {meta: string, score: number}) => {
            if(typeof finalResult === "string") {
                this.setError(finalResult);
            }   else {
                this.status.next({
                    ...this.status.value,
                    success: true,
                    game: null,
                    finalResult: finalResult.meta,
                    score: finalResult.score,
                    loading: false
                });

                clearInterval(this.interval);
            }
        });
    }

    submitResult (): void {

        this.addScoreGQL.mutate({gameToken: this.status.value.finalResult})
        .pipe(map(result => result.data.addScore),
        catchError(err => of(handleErr(err))))
        .subscribe((finalResult: {position: string; score: Score} | string) => {

            if(typeof finalResult === "string") {
                this.setError(finalResult);
            }   else {

                if(finalResult.position !== "1000+") {
                    this.scoreService.insertOne({
                        ...finalResult.score,
                        date: new Date(finalResult.score.date)
                    }, +finalResult.position);
                }
    
                this.status.next({
                    ...this.status.value,
                    info: finalResult.position
                });
            }
        });
    }

    updateCounter (): void {

        this.interval = setInterval(() => {
            this.status.next({
                ...this.status.value,
                counter: this.status.value.counter + 1
            });
        }, 1000);
    }

}