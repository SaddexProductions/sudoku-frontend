import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'time'
})
export class TimePipe implements PipeTransform {

  transform(value: number, ...args: unknown[]): string {

    let seconds = value;
    let minutes = 0;
    let hours = 0;
    let days = 0;

    if(seconds >= 60) {
      minutes = Math.floor(seconds / 60);
    }

    if(minutes >= 60) {
      hours = Math.floor(minutes / 60);
    }

    if(hours >= 24) {
      days = Math.floor(hours / 24);
    }
    
    return `${days > 0 ? days + "d " : ""}${hours > 0 || days > 0 ? (hours % 24) + "h " : ""}
    ${minutes > 0 || hours > 0 || days > 0 ? (minutes % 60) + "m " : ""}${seconds % 60}`;
  }

}
