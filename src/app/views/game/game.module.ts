import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { MatSliderModule } from '@angular/material/slider';
import { GameComponent } from './game.component';
import { AuthGuard } from '../auth/auth.guard';
import { SharedModule } from '../../shared/shared.module';
import { ErrorBarComponent } from 'src/app/components/error-bar/error-bar.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { TimePipe } from './time.pipe';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
    declarations: [
        ErrorBarComponent,
        GameComponent,
        TimePipe
    ],
    imports: [
        CommonModule,
        FontAwesomeModule,
        ReactiveFormsModule,
        RouterModule.forChild([
            {
                path: '', component: GameComponent, 
                canActivate: [AuthGuard]
            }
        ]),
        MatSliderModule,
        SharedModule
    ]
})
export class GameModule {

}