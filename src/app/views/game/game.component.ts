import { Component, OnDestroy, OnInit } from '@angular/core';
import { faCheckCircle, IconDefinition } from '@fortawesome/free-solid-svg-icons';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { Subscription } from 'rxjs';

//local imports
import { GameService, Game } from './game.service';
import { AuthService, Status } from '../auth/auth.service';
import { typeRangeValidator } from './validators/typeValidator';
import sudokuFieldValidator from './validators/sudokuFieldValidator';
import { getX, getY } from './validators/utility/utility';
import { trigger, state, style, transition, animate, keyframes } from '@angular/animations';
import { UIService } from 'src/app/components/menu/ui.service';

export interface Board {
  value: string;
  cellError: boolean;
  rowError: boolean;
  columnError: boolean;
}

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css'],
  animations: [
    trigger('expand', [
      state('in',
        style({ transform: 'scale(1)' })
      ),
      transition('void => *', [
        animate("0.8s ease", keyframes([
          style({ transform: 'scale(0)', offset: 0 }),
          style({ transform: 'scale(1.2)', offset: 0.7 }),
          style({ transform: 'scale(1)', offset: 0.8 })
        ]))
      ])
    ])
  ]
})
export class GameComponent implements OnInit, OnDestroy {

  authStatus: Status = null;
  private authSub: Subscription;
  base: number = 3;
  boardUI: Board[][] = [[]];
  counter: number = 0;
  darkMode: boolean = false;
  error: string;
  faCheckCircle: IconDefinition = faCheckCircle;
  first: boolean = false;
  info: string = "";
  height: number = window.innerHeight;
  game: Game = null;
  gameForm: FormGroup;
  private gameSub: Subscription;
  loading: boolean = false;
  score: number = 0;
  success: boolean;
  private uiSub: Subscription;

  chars: string[] = [" ", "1", "2", "3", "4", "5",
    "6", "7", "8", "9", "0", "A", "B", "C", "D", "E", "F",
    "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q",
    "R", "S", "T", "U", "V", "W", "X", "Y", "Z"];

  constructor(private authService: AuthService,
    private gameService: GameService,
    private titleService: Title,
    private uiService: UIService) {
  }

  ngOnInit(): void {

    this.uiSub = this.uiService.status.subscribe(({ height }) => {

      this.height = height;
    });

    this.gameSub = this.gameService.status.subscribe(status => {
      this.loading = status.loading;
      this.game = status.game;
      this.error = status.error;
      this.score = status.score;
      this.info = status.info;
      this.counter = status.counter;
      this.success = status.success;

      if (this.success) {
        this.titleService.setTitle(`You won - Sudoku`);
      }

      if (this.error) {
        setTimeout(() => this.gameService.resetError(), 3500);
      }

      if (this.info) {
        setTimeout(() => {
          this.gameService.stopGame();
        }, 3500);
      }

      if (this.game) {

        this.base = status.base;

        if (this.boardUI[0].length === 0) {

          this.boardUI = this.game.board.map((value: number[]) => {
            return value.map((innerValue: number) => ({
              value: this.chars[innerValue],
              cellError: false,
              rowError: false,
              columnError: false
            })
            );
          });

          this.initForm();
        }
        this.titleService.setTitle(`Running ${this.base}x${this.base} session - Sudoku`);

      } else {
        this.titleService.setTitle("Start new game - Sudoku");
      }
    });

    this.authSub = this.authService.status.subscribe((status: Status) => {
      this.authStatus = status;
      if (this.authStatus && this.authStatus.user) {

        if (!this.first) {
          this.base = this.authStatus.user.defaultSize;
          this.first = true;
        }

        this.darkMode = status.user.darkMode;
      }
    });
  }

  changed(e: KeyboardEvent): void {

    const transformedTarget = (<HTMLInputElement>e.target);

    const x = getX(transformedTarget.id);
    const y = getY(transformedTarget.id);

    if (this.boardUI[x][y].value.length > transformedTarget.value.length) {
      this.boardUI[x][y].value = transformedTarget.value;
    }

    if (this.gameForm.valid) {

      this.editBoard();
      this.gameService.submitForm();
    }
  }

  editBoard(): void {

    const newArray: number[][] = [[]];

    for (const key in this.gameForm.getRawValue()) {

      const x = getX(key);
      const y = getY(key);

      if (!newArray[x]) newArray.push([]);

      let index = this.chars.indexOf(this.gameForm.getRawValue()[key] ?
        this.gameForm.getRawValue()[key].toUpperCase() : this.gameForm.getRawValue()[key]);
      if (index < 0) index = 0;

      newArray[x][y] = index;
    }

    this.gameService.editBoard(newArray);
  }

  initForm(): void {

    const obj = {};

    for (let i = 0; i < this.boardUI.length; i++) {
      for (let j = 0; j < this.boardUI[i].length; j++) {
        obj['field_' + i + '-' + j] = new FormControl({
          value: this.boardUI[i][j].value !== " " ?
            this.boardUI[i][j].value : null, disabled: this.game.original[i][j] > 0
        }
        );
      }
    }

    this.gameForm = new FormGroup(obj);

    for (const key in this.gameForm.controls) {
      this.gameForm.controls[key].setValidators([
        Validators.required,
        Validators.maxLength(1),
        typeRangeValidator(this.chars.slice(1, (this.base * this.base) + 1)),
        sudokuFieldValidator(this.boardUI, this.base, this.gameForm)
      ]);
    }
  }

  startGame(): void {
    this.gameService.startGame(this.base);
  }

  stopGame(): void {
    this.gameForm = null;
    this.boardUI = [[]];
    this.gameService.stopGame();
  }

  submitResultToServer(): void {
    this.gameService.submitResult();
  }

  ngOnDestroy(): void {
    if (this.gameSub) this.gameSub.unsubscribe();
    if (this.gameForm && this.game) this.editBoard();
    this.authSub.unsubscribe();
    this.uiSub.unsubscribe();

    this.gameService.resetError();
  }

}
