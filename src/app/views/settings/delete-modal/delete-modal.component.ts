import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../../auth/auth.service';
import { faExclamationTriangle, IconDefinition } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-delete-modal',
  templateUrl: './delete-modal.component.html',
  styleUrls: ['./delete-modal.component.css']
})
export class DeleteModalComponent implements OnInit {

  @Output() closeModal: EventEmitter<void> = new EventEmitter();
  @Input() darkMode: boolean = false;
  @Input() error: string = "";
  faExclamationTriangle: IconDefinition = faExclamationTriangle;
  passwordForm: FormGroup;

  constructor(private authService: AuthService) { }

  ngOnInit(): void {
    this.initForm();
  }

  private initForm (): void {
    this.passwordForm = new FormGroup({
      'password': new FormControl(null, Validators.required)
    });
  }

  close (): void {
    this.closeModal.emit();
  }

  deleteAccount (): void {
    if (!this.passwordForm.valid) return;

    this.authService.deleteAccount(this.passwordForm.controls.password.value);
  }

}
