import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';
import { SettingsComponent } from './settings.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthGuard } from '../auth/auth.guard';
import { CheckboxComponent } from 'src/app/components/checkbox/checkbox.component';
import { MatSliderModule } from '@angular/material/slider';
import { SettingsErrorComponent } from './settings-error/settings-error.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { DeleteModalComponent } from './delete-modal/delete-modal.component';

@NgModule({
    declarations: [
        CheckboxComponent,
        SettingsComponent,
        SettingsErrorComponent,
        DeleteModalComponent
    ],
    imports: [
        CommonModule,
        FontAwesomeModule,
        FormsModule,
        MatSliderModule,
        ReactiveFormsModule,
        RouterModule.forChild([
            {
                path: '', component: SettingsComponent,
                canActivate: [AuthGuard]
            }
        ]),
        SharedModule
    ]
})
export class SettingsModule {
}