import { Component, OnDestroy, OnInit } from '@angular/core';
import { AuthService, Status, EditUserArgument } from '../auth/auth.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { User } from 'src/generated/graphql';
import { Subscription } from 'rxjs';
import { Title } from '@angular/platform-browser';
import { equalPasswordsValidator } from 'src/app/shared/equalPasswords';
import { faCheckCircle, IconDefinition } from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router';
import { UIService } from 'src/app/components/menu/ui.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit, OnDestroy {

  private authSub: Subscription;
  changed: boolean = false;
  deleteError: string = "";
  faCheckCircle: IconDefinition = faCheckCircle;
  error: string = "";
  info: string = "";
  height: number = window.innerHeight;
  loading: boolean = false;
  modal: boolean = false;
  settingsForm: FormGroup;
  user: User = null;
  private uiSub: Subscription;

  constructor(private authService: AuthService, 
    private titleService: Title,
    private router: Router,
    private uiService: UIService) {
  }

  ngOnInit (): void {
    this.titleService.setTitle("User Settings - Sudoku");

    this.authSub = this.authService.status.subscribe((status: Status) => {

      this.user = {
        ...status.user 
      }

      this.info = status.info;

      if(this.info) {
        setTimeout(() => {
          this.authService.resetInfo();
          this.router.navigate(['/']);
        }, 3000);
      }

      this.loading = status.loading;
      this.error = status.error;
      this.deleteError = status.deleteError;

      if(this.error) this.settingsForm.controls.currentPassword.reset();

      if (!this.settingsForm) {
        this.initForm();
      } else {
        this.settingsForm.controls.darkMode.setValue(status.error ? this.settingsForm.value.darkMode : status.user.darkMode);
      }
    });

    this.uiSub = this.uiService.status.subscribe(({height}) => {

      this.height = height;
    });
  }

  ngOnDestroy (): void {
    this.authSub.unsubscribe();
    this.authService.resetError();
    this.uiSub.unsubscribe();
    this.authService.resetDeleteError();
  }

  changeSlider (value: number): void {
    this.settingsForm.controls.size.setValue(value);
    this.checkIfChanged();
  }

  checkIfChanged (): void {

    let changed: boolean = false;

    const formObject = {
      ...this.user,
      username: this.settingsForm.controls.username.value,
      darkMode: this.settingsForm.controls.darkMode.value,
      defaultSize: this.settingsForm.controls.size.value
    }

    for(const key in formObject) {
      changed = formObject[key] !== this.user[key] || changed;
    }

    if(this.settingsForm.controls.password.value) changed = true;

    this.changed = changed;
  }

  closeModal (): void {
    this.modal = false;
    this.authService.resetDeleteError();
  }

  private initForm(): void {
    this.settingsForm = new FormGroup({
      'currentPassword': new FormControl(null, [Validators.required]),
      'password': new FormControl(null, 
        [Validators.minLength(6), Validators.maxLength(25),
          Validators.pattern(/^(?:(?=.*[a-z])(?:(?=.*[A-Z])(?=.*[\d\W])|(?=.*\W)(?=.*\d))|(?=.*\W)(?=.*[A-Z])(?=.*\d)).{5,25}$/)]),
      'repeatPassword': new FormControl(null),
      'username': new FormControl(this.user.username, [Validators.required, 
        Validators.minLength(3), 
        Validators.maxLength(20),
      Validators.pattern(/^(?=.{3,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$/)],
      [this.authService.usernameAsyncValidator]),
      'darkMode': new FormControl(this.user.darkMode),
      "size": new FormControl(this.user.defaultSize, [Validators.required, Validators.min(3), Validators.max(6)])
    }, equalPasswordsValidator);
  }

  saveSettings (): void {

    if(this.settingsForm.invalid || !this.changed) return;

    const objToSend: EditUserArgument = {
      password: this.settingsForm.value.currentPassword,
      newPassword: this.settingsForm.value.password,
      darkMode: this.settingsForm.value.darkMode !== this.user.darkMode ?
      this.settingsForm.value.darkMode : null,
      size: this.settingsForm.value.size !== this.user.defaultSize ?
      this.settingsForm.value.size : null,
      username: this.settingsForm.value.username !== this.user.username ?
      this.settingsForm.value.username : null
    }

    for(const key in objToSend) {
      if(!objToSend[key] && typeof objToSend[key] !== "boolean") {
        delete objToSend[key];
      }
    }

    this.authService.resetError();

    this.authService.editUser(objToSend);

  }

  toggleCheck (value: boolean): void {
    this.settingsForm.controls.darkMode.setValue(value);
    this.checkIfChanged();
  }

}
