import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingsErrorComponent } from './settings-error.component';

describe('SettingsErrorComponent', () => {
  let component: SettingsErrorComponent;
  let fixture: ComponentFixture<SettingsErrorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SettingsErrorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsErrorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
