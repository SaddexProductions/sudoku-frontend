import { Component, Input } from '@angular/core';
import { faExclamationTriangle, IconDefinition } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-settings-error',
  templateUrl: './settings-error.component.html',
  styleUrls: ['./settings-error.component.css']
})
export class SettingsErrorComponent {

  @Input() error: string = "";
  faExclamationTriangle: IconDefinition = faExclamationTriangle;

}
