import { Component, OnInit, OnDestroy } from '@angular/core';
import { ScoreService, ScoreStatus } from './scoreboard.service';
import { Subscription } from 'rxjs';
import { faExclamationTriangle, IconDefinition } from '@fortawesome/free-solid-svg-icons';
import { Title } from '@angular/platform-browser';
import { AuthService } from '../auth/auth.service';
import { UIService } from 'src/app/components/menu/ui.service';

@Component({
  selector: 'app-scoreboard',
  templateUrl: './scoreboard.component.html',
  styleUrls: ['./scoreboard.component.css']
})
export class ScoreboardComponent implements OnInit, OnDestroy {

  all: boolean = false;
  private authSub: Subscription;
  darkMode: boolean = false;
  error: string = "";
  height: number = window.innerHeight;
  faExclamationTriangle: IconDefinition = faExclamationTriangle;
  loading: boolean = false;
  scores = [];
  private scoreSubscription: Subscription;
  private uiSub: Subscription;

  constructor(private authService: AuthService, 
    private scoreService: ScoreService,
    private titleService: Title,
    private uiService: UIService) {
  }

  ngOnInit(): void {
    this.scoreService.getScores();
    this.titleService.setTitle("Top 1000 Scoreboard - Sudoku");

    this.authSub = this.authService.status.subscribe(({user}) => {
        this.darkMode = user && user.darkMode;
    });

    this.scoreSubscription = this.scoreService.status.subscribe(
      (status: ScoreStatus) => {
        this.loading = status.loading;
        this.scores = status.scores;
        this.all = status.all;
        this.error = status.error;
      }
    );

    this.uiSub = this.uiService.status.subscribe(({height}) => {

      this.height = height;
    });
  }

  ngOnDestroy(): void {
    this.scoreSubscription.unsubscribe();
    this.authSub.unsubscribe();
    this.uiSub.unsubscribe();
    this.scoreService.resetError();
  }

  loadScores(): void {
    this.scoreService.getScores();
  }

}
