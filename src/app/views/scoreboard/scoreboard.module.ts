import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ScoreboardComponent } from './scoreboard.component';
import { SharedModule } from '../../shared/shared.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { UsernameShortenerPipe } from './username-shortener.pipe';

@NgModule({
    declarations: [
        ScoreboardComponent,
        UsernameShortenerPipe
    ],
    imports: [
        CommonModule,
        FontAwesomeModule,
        RouterModule.forChild([
            {
                path: '', component: ScoreboardComponent
            }
        ]),
        SharedModule
    ]
})
export class ScoreBoardModule {
}