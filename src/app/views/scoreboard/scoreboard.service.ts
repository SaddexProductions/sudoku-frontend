import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { GetScoresGQL, GetScoresQuery } from 'src/generated/graphql';
import { map, catchError } from 'rxjs/operators';
import { handleErr } from 'src/app/shared/handleError';

interface Score {
    _id: string;
    score: number;
    size: number;
    username: string;
    date: Date;
}

export interface ScoreStatus {
    all: boolean;
    loading: boolean;
    scores: Score[];
    error: string;
    loadedOnce: boolean;
}

@Injectable({ providedIn: 'root' })
export class ScoreService {

    readonly status = new BehaviorSubject<ScoreStatus>({
        all: false,
        loading: false,
        scores: [],
        error: null,
        loadedOnce: false
    });
    scoreQuery: Observable<GetScoresQuery['scores']>;

    constructor(private getScoresGQL: GetScoresGQL) {
    }

    clearScores(): void {
        this.status.next({
            ...this.status.value,
            error: null,
            loadedOnce: false,
            scores: [],
            all: false
        });
    }

    getScores(): void {

        this.status.next({
            ...this.status.value,
            error: null,
            loading: true
        });

        this.getScoresGQL.fetch({ start: this.status.value.scores.length })
            .pipe(map(firstResult => firstResult.data.scores.map(score => ({
                ...score,
                date: new Date(score.date)
            })
            )
            ), catchError(err => of(handleErr(err)))).subscribe(result => {

                if (typeof result === "string") {
                    this.status.next({
                        ...this.status.value,
                        loading: false,
                        error: result
                    })
                } else {
                    this.status.next({
                        all: this.status.value.scores.length + result.length > 900 || result.length < 100,
                        loading: false,
                        error: null,
                        loadedOnce: true,
                        scores: [
                            ...this.status.value.scores,
                            ...result
                        ]
                    });
                }
            });
    }

    insertOne(score: Score, position: number): void {

        if (!this.status.value.loadedOnce) return;

        const scores = [...this.status.value.scores];

        scores.splice(position - 1, 0, score);

        this.status.next({
            ...this.status.value,
            scores
        });
    }

    resetError(): void {
        this.status.next({
            ...this.status.value,
            error: null
        })
    }
}