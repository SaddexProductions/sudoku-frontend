import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'usernameShortener'
})
export class UsernameShortenerPipe implements PipeTransform {

  transform(value: string): string {
    
    return window.innerWidth > 400 || value.length < 14 ? value : value.substr(0, 11) + "...";
  }

}
