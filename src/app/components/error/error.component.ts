import { Component, Input } from '@angular/core';
import { faExclamationTriangle, faCheckCircle, IconDefinition } from '@fortawesome/free-solid-svg-icons';
import { transition, trigger, state, animate, style } from '@angular/animations';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.css'],
  animations: [
    // the fade-in/fade-out animation.
    trigger('simpleFadeAnimation', [

      // the "in" style determines the "resting" state of the element when it is visible.
      state('in', style({opacity: 1})),

      // fade in when created. this could also be written as transition('void => *')
      transition(':enter', [
        style({opacity: 0}),
        animate(600 )
      ]),

      // fade out when destroyed. this could also be written as transition('void => *')
      transition(':leave',
        animate(600, style({opacity: 0})))
    ])
  ]
})
export class ErrorComponent {

  @Input() error: boolean = true;
  @Input() errorMessage: string = "";
  @Input() darkMode: boolean = null;
  faCheckCircle: IconDefinition = faCheckCircle;
  faExclamationTriangle: IconDefinition = faExclamationTriangle;

}
