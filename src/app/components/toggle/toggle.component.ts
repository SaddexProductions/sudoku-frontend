import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-toggle',
  templateUrl: './toggle.component.html',
  styleUrls: ['./toggle.component.css']
})
export class ToggleComponent {

  @Input() enabled: boolean;
  @Output() toggleEvent: EventEmitter<boolean> = new EventEmitter();

  toggle () {
    this.toggleEvent.emit(!this.enabled);
  }

}
