import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MadebyComponent } from './madeby.component';

describe('MadebyComponent', () => {
  let component: MadebyComponent;
  let fixture: ComponentFixture<MadebyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MadebyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MadebyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
