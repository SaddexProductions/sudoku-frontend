import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

export interface UIStatus {
    menu: boolean;
    subMenu: boolean;
    height: number;
}

@Injectable({providedIn: 'root'})
export class UIService {

    readonly status: BehaviorSubject<UIStatus> = new BehaviorSubject({
        menu: false,
        subMenu: false,
        height: window.innerHeight
    });

    setMenu(value: boolean): void {
        this.status.next({
            ...this.status.value,
            menu: value
        });
    }

    setSubMenu(value: boolean): void {
        this.status.next({
            ...this.status.value,
            subMenu: value
        });
    }

    setHeight (height: number) {
        this.status.next({
            ...this.status.value,
            height
        })
    }

}