import { Component, Output, EventEmitter, OnDestroy, Input, OnInit } from '@angular/core';
import { UIService, UIStatus } from './ui.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { faChevronDown, 
  faChevronUp, 
  IconDefinition} from '@fortawesome/free-solid-svg-icons';
import { trigger, transition, style, animate, state } from '@angular/animations';
import { AuthService, Status } from 'src/app/views/auth/auth.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css'],
  animations: [
    trigger('slideInOut', [
      state('in',
        style({ transform: 'translateX(0%)'})
      ),
      transition('void => *', [
        style({
          transform: 'translateX(-100%)'
        }),
        animate('400ms ease')
      ]),
      transition('* => void', [
        animate('400ms ease', style({
          transform: 'translateX(-100%)'
        }))
      ])
    ])
  ]
})
export class MenuComponent implements OnInit, OnDestroy {

  @Output() clickBackground: EventEmitter<null> = new EventEmitter();
  private authSub: Subscription;
  private menuSub: Subscription;
  private routeSub: Subscription;
  darkMode: boolean;
  faChevronDown: IconDefinition = faChevronDown;
  faChevronUp: IconDefinition = faChevronUp;
  height: number = window.innerHeight;
  login: boolean = false;
  menu: boolean = false;
  @Input() settings: boolean = false;
  @Input() enabled: boolean = false;
  subMenu: boolean = false;

  emitEvent () {
    this.clickBackground.emit(null);
  }

  constructor(private uiService: UIService, 
    private authService: AuthService, 
    private router: Router) {
  }

  ngOnInit (): void {
    this.menuSub = this.uiService.status.subscribe((status: UIStatus) => {
      this.menu = status.menu;
      this.height = status.height;
      this.subMenu = status.subMenu;
    });

    this.routeSub = this.router.events.subscribe(() => {
      this.uiService.setMenu(false);
      this.uiService.setSubMenu(false);
    });

    this.authSub = this.authService.status.subscribe((status: Status) => {
      this.login = status.login;
      
      this.darkMode = status.user && status.user.darkMode;
    });
  }

  ngOnDestroy (): void {
    this.authSub.unsubscribe();
    this.menuSub.unsubscribe();
    this.routeSub.unsubscribe();
  }

  closeMenu (): void {
    this.uiService.setMenu(false);
    this.uiService.setSubMenu(false);
  }

  logout (): void {
    this.authService.logout();
  }

  toggleDarkMode (mode: boolean): void {

    this.authService.toggleDarkMode(mode);
  }

  toggleSubMenu (): void {
    this.uiService.setSubMenu(!this.subMenu);
  }

}
