import { Component, Input } from '@angular/core';
import { faExclamationTriangle, IconDefinition } from '@fortawesome/free-solid-svg-icons';
import { trigger, transition, style, animate, state } from '@angular/animations';

@Component({
  selector: 'app-error-bar',
  templateUrl: './error-bar.component.html',
  styleUrls: ['./error-bar.component.css'],
  animations: [
    trigger('heightSlide', [
      state('in',
        style({ height: '40px' })
      ),
      transition('void => *', [
        style({
          height: '0px'
        }),
        animate('400ms ease')
      ]),
      transition('* => void', [
        animate('400ms ease', style({
          height: '0px'
        }))
      ])
    ])
  ]
})
export class ErrorBarComponent {

  @Input() darkMode: boolean = false;
  @Input() error: string;
  faExclamationTriangle: IconDefinition = faExclamationTriangle;

}
