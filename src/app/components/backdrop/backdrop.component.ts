import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-backdrop',
  templateUrl: './backdrop.component.html',
  styleUrls: ['./backdrop.component.css']
})
export class BackdropComponent {

  @Output() clickEmitter: EventEmitter<null> = new EventEmitter();

  passClick(): void {
    this.clickEmitter.emit(null);
  }

}
