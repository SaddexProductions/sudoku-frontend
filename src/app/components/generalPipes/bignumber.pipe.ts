import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'bignumber'
})
export class BignumberPipe implements PipeTransform {

  transform(value: number): string {
    
    const splitted = value.toString().split("").reverse();

    let added = 0;

    for(let i = 0; i < splitted.length; i++) {

      if((i - added) % 3 === 0 && i !== 0) {

        added++;
        splitted.splice(i, 0, " ");
        i++;
      }
    }

    return splitted.reverse().join("");
  }

}
