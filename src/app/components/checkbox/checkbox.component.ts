import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.css']
})
export class CheckboxComponent {
  
  @Input() enabled: boolean = false;
  @Output() toggle: EventEmitter<boolean> = new EventEmitter();

  toggleCheck () {
    this.toggle.emit(!this.enabled);
  }
}
