import { Component, OnDestroy, OnInit } from '@angular/core';
import { AuthService, Status } from 'src/app/views/auth/auth.service';
import { User } from 'src/app/views/auth/user.model';
import { Subscription } from 'rxjs';
import { Router, NavigationEnd } from '@angular/router';
import {faBars, faChevronDown, faChevronUp, IconDefinition} from '@fortawesome/free-solid-svg-icons';
import { UIService, UIStatus } from '../menu/ui.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {

  faBars = faBars;
  faChevronDown: IconDefinition = faChevronDown;
  faChevronUp: IconDefinition = faChevronUp;
  hamburgerMenu: boolean = false;
  login: boolean = false;
  menuSub: Subscription;
  notAuth: boolean = true;
  routeSub: Subscription;
  settings: boolean;
  subMenu: boolean = false;
  user: User = null;
  userSub: Subscription;

  constructor(private authService: AuthService, 
    private uiService: UIService, 
    private router: Router) {
  }

  ngOnInit (): void {
    
    this.settings = this.router.url.indexOf("settings") > -1;

    //backup in-case the notAuth line below isn't triggered on page load
    this.notAuth = window.location.href.indexOf("auth") < window.location.href.length - 4 
    || window.location.href === "/" && this.login;

    this.routeSub = this.router.events.subscribe(route => {

      if(route instanceof NavigationEnd) {
        this.notAuth = route.url.indexOf("auth") < route.url.length - 4 || route.url === "/" && this.login;

        this.settings = route.url.indexOf("settings") > -1;

        this.uiService.setSubMenu(false);
      }
    });

    this.userSub = this.authService.status.subscribe((status: Status)=> {
      this.user = status.user;
      this.login = status.login;
    });

    this.menuSub = this.uiService.status.subscribe((menu: UIStatus) => {
      this.hamburgerMenu = menu.menu;
      this.subMenu = menu.subMenu;
    });
  }

  closeMenu (): void {
    this.uiService.setMenu(false);
    this.uiService.setSubMenu(false);
  }

  logout (): void {
    this.authService.logout();
  }

  toggleDarkMode(mode: boolean): void {
    this.authService.toggleDarkMode(mode);
  }

  toggleMenu (): void {
    this.uiService.setMenu(!this.hamburgerMenu);
  }

  toggleSubMenu (): void {
    this.uiService.setSubMenu(!this.subMenu);
  }

  ngOnDestroy (): void {
    this.menuSub.unsubscribe();
    this.routeSub.unsubscribe();
    this.userSub.unsubscribe();
  }

  sizeClose (): void {

    this.uiService.setHeight(window.innerHeight);

    if(window.innerWidth >= 900 && this.hamburgerMenu) {
      this.closeMenu();
    }
  }

}
